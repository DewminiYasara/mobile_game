package com.example.jimmyonajourney_riddlegame;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.btn_start).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent level1Intent = new Intent(MainActivity.this, LevelsActivity.class);
                level1Intent.putExtra("level",1);
                level1Intent.putExtra("marks",0);
                startActivity(level1Intent);
                finish();
            }
        });

        findViewById(R.id.btn_help).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent helpIntent = new Intent(MainActivity.this, HelpActivity.class);
                startActivity(helpIntent);
                finish();
            }
        });

        Intent intent = getIntent();
        final int level = intent.getIntExtra("level",1);
        final int marks = intent.getIntExtra("marks",0);

        findViewById(R.id.btn_resume).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.super.onResume();
                Intent resumeIntent = new Intent(MainActivity.this, LevelsActivity.class);
                resumeIntent.putExtra("level",level);
                resumeIntent.putExtra("marks",marks);
                startActivity(resumeIntent);
                finish();
            }
        });

        findViewById(R.id.btn_exit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
