package com.example.jimmyonajourney_riddlegame;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class RiddleActivity extends AppCompatActivity {

    public int marks;

    public String[] questions1 =
            {"Adriana's mom had four kids: Marta, Anna, Justina...","A farmer had 10 sheep, all but 7 died, how many live sheep were left?"
                    ,
                    "If there are 100000000 bricks on a plane, and one falls off, how many are left?"};

    public String[] questions2 =
            {"Imagine you're in a dark room that is perfectly empty with nothing in it. There are no windows or doors. What is the easiest way to escape?",
                    "What is at the end of a rainbow?",
                    "What travels around the world but stays in one corner?"};

    public String[] questions3 =
            {"In a circular castle, there is a king, a queen, a prince, a chef, a maid, and a butler. One day the prince is murdered. The three suspects are the chef, the maid, and the butler. The chef said he was cooking dinner. The maid said she was sweeping the corners. The butler said that he was greeting guests. Who killed the prince?",
                    "Which is correct to say, \"The yolk of an egg are white\" or \"The yolk of an egg is white\"?",
                    "Poor people have it. Rich people need it. If you eat it you die. What is it?"};

    public String[] questions4 =
            {"A man pushed his car past a hotel, only to realize he was bankrupt. How did he figure this out?",
                    "I don't speak, I cannot hear, but I always tell the truth.", "How many months have 28 days?"};

    public String[] questions5 =
            {"Divide 30 by half and add 10, what have you got?",
                    "If a farmer has 5 haystacks in one field and 4 haystacks in the other field, how many haystacks would he have if he combined them all in another field?",
                    "A man is in a maze, suddenly the power shuts off. He walks on and it gets colder and colder. In front of him are two doors. In one is a loaded gun that will shoot him and the other contains an electric chair that he will be strapped to. Which one should he choose?"};

    public String[][] choices1 =
            {{"Annabell","Chloe","Adriana"},
                    {"7","None","3"},
                    {"99999999","1","0"}};

    public String[][] choices2 =
            {{"Stop imagining you are in that room","Wait","Dig a hole in the ground with your bare hands"},
                    {"Nothing","W","Light"},
                    {"People","Donkeys","Stamps"}};

    public String[][] choices3 =
            {{"The chef","The maid","Nobody, he committed suicide"},
                    {"The yolk of an egg are white","The yolk of an egg is white","Neither"},
                    {"Nothing","Money","Poison"}};

    public String[][] choices4 =
            {{"His pockets were empty","He was playing a board game","There was a text alert"},
                    {"The Mirror","The Watch","Old Granny"},
                    {"1","Depends if there is a leap year or not","12"}};

    public String[][] choices5 =
            {{"70","60","25"},
                    {"9","1","4"},
                    {"The room with the electric chair","The room with a gun","He should just go back"}};

    public String[] answers1 =
            {"Adriana","7","99999999"};
    public String[] answers2 =
            {"Stop imagining you are in that room","W","Stamps"};
    public String[] answers3 =
            {"The maid","Neither","Nothing"};
    public String[] answers4 =
            {"He was playing a board game","The Mirror","12"};
    public String[] answers5 =
            {"70","1","The room with the electric chair"};

    public String answer;

    public String getQuestion1(int a){
        return questions1[a];
    }

    public String getQuestion2(int a){
        return questions2[a];
    }

    public String getQuestion3(int a){
        return questions3[a];
    }

    public String getQuestion4(int a){
        return questions4[a];
    }

    public String getQuestion5(int a){
        return questions5[a];
    }

    public String[] getChoices1(int a){
        return choices1[a];
    }

    public String[] getChoices2(int a){
        return choices2[a];
    }

    public String[] getChoices3(int a){
        return choices3[a];
    }

    public String[] getChoices4(int a){
        return choices4[a];
    }

    public String[] getChoices5(int a){
        return choices5[a];
    }

    public String getAnswer1(int a){
        return answers1[a];
    }

    public String getAnswer2(int a){
        return answers2[a];
    }

    public String getAnswer3(int a){
        return answers3[a];
    }

    public String getAnswer4(int a){
        return answers4[a];
    }

    public String getAnswer5(int a){
        return answers5[a];
    }

    public void correctAnswer(int level){
        Toast.makeText(getApplicationContext(), "Your answer is correct!", Toast.LENGTH_LONG).show();
        if(level>=5) {
            Intent endIntent = new Intent(RiddleActivity.this, EndingActivity.class);
            endIntent.putExtra("marks",marks);
            endIntent.putExtra("status",1);
            startActivity(endIntent);
            finish();
        }else{
            Intent nlIntent = new Intent(RiddleActivity.this, LevelsActivity.class);
            nlIntent.putExtra("level",level+1);
            nlIntent.putExtra("marks",marks);
            startActivity(nlIntent);
            finish();
        }
    }

    public void wrongAnswer(int level){
        Toast.makeText(getApplicationContext(), "Your answer is incorrect!", Toast.LENGTH_LONG).show();
        if(marks<=0||level<=1){
            Intent endIntent = new Intent(RiddleActivity.this, EndingActivity.class);
            if(marks<0){marks=0;}
            endIntent.putExtra("marks",marks);
            endIntent.putExtra("status",0);
            startActivity(endIntent);
            finish();
        }else {
            Intent plIntent = new Intent(RiddleActivity.this, LevelsActivity.class);
            plIntent.putExtra("level", level - 1);
            plIntent.putExtra("marks",marks);
            startActivity(plIntent);
            finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_riddle);

        Intent intent = getIntent();
        final int level = intent.getIntExtra("level",0);
        marks = intent.getIntExtra("marks",0);

        TextView txtlevel = findViewById(R.id.txt_level);
        TextView question = findViewById(R.id.txt_riddle);
        final Button btn1 = findViewById(R.id.btn_ans1);
        final Button btn2 = findViewById(R.id.btn_ans2);
        final Button btn3 = findViewById(R.id.btn_ans3);

        int x = new Random().nextInt(2);

        if(level==1){
            txtlevel.setText(getResources().getString(R.string.level1));
            question.setText(getQuestion1(x));
            btn1.setText(getChoices1(x)[0]);
            btn2.setText(getChoices1(x)[1]);
            btn3.setText(getChoices1(x)[2]);
            answer = getAnswer1(x);
        }else if(level==2){
            txtlevel.setText(getResources().getString(R.string.level2));
            question.setText(getQuestion2(x));
            btn1.setText(getChoices2(x)[0]);
            btn2.setText(getChoices2(x)[1]);
            btn3.setText(getChoices2(x)[2]);
            answer = getAnswer2(x);
        }else if(level==3) {
            txtlevel.setText(getResources().getString(R.string.level3));
            question.setText(getQuestion3(x));
            btn1.setText(getChoices3(x)[0]);
            btn2.setText(getChoices3(x)[1]);
            btn3.setText(getChoices3(x)[2]);
            answer = getAnswer3(x);
        }else if(level==4) {
            txtlevel.setText(getResources().getString(R.string.level4));
            question.setText(getQuestion4(x));
            btn1.setText(getChoices4(x)[0]);
            btn2.setText(getChoices4(x)[1]);
            btn3.setText(getChoices4(x)[2]);
            answer = getAnswer4(x);
        }else {
            txtlevel.setText(getResources().getString(R.string.level5));
            question.setText(getQuestion5(x));
            btn1.setText(getChoices5(x)[0]);
            btn2.setText(getChoices5(x)[1]);
            btn3.setText(getChoices5(x)[2]);
            answer = getAnswer5(x);
        }

        Toast.makeText(getApplicationContext(), "Select an Answer", Toast.LENGTH_SHORT).show();

        findViewById(R.id.btn_ans1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(btn1.getText().equals(answer)){
                    marks += 20;
                    correctAnswer(level);
                }else{
                    Toast.makeText(getApplicationContext(), "Your answer is incorrect! Try Again!", Toast.LENGTH_LONG).show();

                    findViewById(R.id.btn_ans2).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(btn2.getText().equals(answer)){
                                marks += 10;
                                correctAnswer(level);
                            }else{
                                marks -= 20;
                                wrongAnswer(level);
                            }
                        }
                    });

                    findViewById(R.id.btn_ans3).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(btn3.getText().equals(answer)){
                                marks += 10;
                                correctAnswer(level);
                            }else{
                                marks -= 20;
                                wrongAnswer(level);
                            }
                        }
                    });
                }
            }
        });

        findViewById(R.id.btn_ans2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(btn2.getText().equals(answer)){
                    marks += 20;
                    correctAnswer(level);
                }else{
                    Toast.makeText(getApplicationContext(), "Your answer is incorrect! Try Again!", Toast.LENGTH_LONG).show();

                    findViewById(R.id.btn_ans1).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(btn1.getText().equals(answer)){
                                marks += 10;
                                correctAnswer(level);
                            }else{
                                marks -= 20;
                                wrongAnswer(level);
                            }
                        }
                    });

                    findViewById(R.id.btn_ans3).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(btn3.getText().equals(answer)){
                                marks += 10;
                                correctAnswer(level);
                            }else{
                                marks -= 20;
                                wrongAnswer(level);
                            }
                        }
                    });
                }
            }
        });

        findViewById(R.id.btn_ans3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(btn3.getText().equals(answer)){
                    marks += 20;
                    correctAnswer(level);
                }else{
                    Toast.makeText(getApplicationContext(), "Your answer is incorrect! Try Again!", Toast.LENGTH_LONG).show();

                    findViewById(R.id.btn_ans1).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(btn1.getText().equals(answer)){
                                marks += 10;
                                correctAnswer(level);
                            }else{
                                marks -= 20;
                                wrongAnswer(level);
                            }
                        }
                    });

                    findViewById(R.id.btn_ans2).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(btn2.getText().equals(answer)){
                                marks += 10;
                                correctAnswer(level);
                            }else{
                                marks -= 20;
                                wrongAnswer(level);
                            }
                        }
                    });
                }
            }
        });

        findViewById(R.id.imbtn_pause).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RiddleActivity.super.onPause();
                Intent mainIntent = new Intent(RiddleActivity.this,MainActivity.class);
                mainIntent.putExtra("level",level);
                mainIntent.putExtra("marks",marks);
                startActivity(mainIntent);
                finish();
            }
        });
    }
}
