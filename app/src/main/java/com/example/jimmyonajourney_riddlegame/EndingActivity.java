package com.example.jimmyonajourney_riddlegame;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.ValueAnimator;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

public class EndingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ending);

        Intent intent = getIntent();
        final int status = intent.getIntExtra("status",0);
        final int marks = intent.getIntExtra("marks",0);
        final ImageView bgOne = findViewById(R.id.im_bg1);
        final ImageView bgTwo = findViewById(R.id.im_bg2);
        TextView txtTitle = findViewById(R.id.txt_title);


        if(status==0){
            bgOne.setImageResource(R.drawable.lost1);
            bgTwo.setImageResource(R.drawable.lost2);
            txtTitle.setText(getResources().getString(R.string.lostTitle));
        }else{
            bgOne.setImageResource(R.drawable.won1);
            bgTwo.setImageResource(R.drawable.won2);
            txtTitle.setText(getResources().getString(R.string.wonTitle));
        }

        final ValueAnimator animator = ValueAnimator.ofFloat(0.0f, 1.0f);
        animator.setRepeatCount(ValueAnimator.INFINITE);
        animator.setInterpolator(new LinearInterpolator());
        animator.setDuration(10000L);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                final float progress = (float) animation.getAnimatedValue();
                final float width = bgOne.getWidth();
                final float translationX = width * progress;
                bgOne.setTranslationX(translationX);
                bgTwo.setTranslationX(translationX - width);
            }
        });
        animator.start();

        final ImageView boy = findViewById(R.id.im_boy);
        boy.setImageResource(R.drawable.boy_animation);
        final AnimationDrawable boy_run =(AnimationDrawable)boy.getDrawable();
        boy_run.start();

        Timer timer=new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                Intent finalIntent = new Intent(EndingActivity.this, FinalActivity.class);
                finalIntent.putExtra("status",status);
                finalIntent.putExtra("marks",marks);
                startActivity(finalIntent);
                finish();
            }
        },9800);
    }
}
