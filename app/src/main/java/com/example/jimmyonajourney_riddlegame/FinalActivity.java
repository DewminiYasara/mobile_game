package com.example.jimmyonajourney_riddlegame;

import androidx.appcompat.app.AppCompatActivity;;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class FinalActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_final);

        Intent intent = getIntent();
        int status = intent.getIntExtra("status",0);
        int marks = intent.getIntExtra("marks",0);

        int highScore;

        TextView txtTitle = findViewById(R.id.txt_title1);
        TextView txtMessage = findViewById(R.id.txt_message);
        TextView txtMarks = findViewById(R.id.txt_marks);
        TextView txtHighScore = findViewById(R.id.txt_highscore);

        if(status==0)
        {
            txtTitle.setText(getResources().getString(R.string.lostTitle));
            txtMessage.setText(getResources().getString(R.string.lostMessage));
            txtMarks.setText(getResources().getString(R.string.marks,marks));
        }else
        {
            txtTitle.setText(getResources().getString(R.string.wonTitle));
            txtMessage.setText(getResources().getString(R.string.wonMessage));
            txtMarks.setText(getResources().getString(R.string.marks,marks));
        }

        SharedPreferences prefs = this.getSharedPreferences("myPrefsKey", Context.MODE_PRIVATE);
        highScore = prefs.getInt("highScore",0);

        if(highScore>marks){
            txtHighScore.setText(getResources().getString(R.string.highScore,highScore));
        }else
        {
            highScore = marks;
            txtHighScore.setText(getResources().getString(R.string.highScore,highScore));
            prefs.edit().putInt("highScore",highScore).apply();
        }

        findViewById(R.id.btn_playAgain).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mainIntent = new Intent(FinalActivity.this, MainActivity.class);
                startActivity(mainIntent);
                finish();
            }
        });

        findViewById(R.id.btn_exit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
