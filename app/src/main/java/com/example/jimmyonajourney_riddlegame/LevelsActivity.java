package com.example.jimmyonajourney_riddlegame;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.ValueAnimator;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

public class LevelsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_levels);

        Intent intent = getIntent();
        final int level = intent.getIntExtra("level",0);
        final int marks = intent.getIntExtra("marks",0);
        final ImageView bgOne = findViewById(R.id.im_bg1);
        final ImageView bgTwo = findViewById(R.id.im_bg2);
        TextView txtLevel = findViewById(R.id.txt_level);
        TextView txtMarks = findViewById(R.id.txt_marks);

        txtMarks.setText(Integer.toString(marks));

        if(level==1){
            bgOne.setImageResource(R.drawable.spring1);
            bgTwo.setImageResource(R.drawable.spring2);
            txtLevel.setText(getResources().getString(R.string.level1));
        }else if(level==2){
            bgOne.setImageResource(R.drawable.summer1);
            bgTwo.setImageResource(R.drawable.summer2);
            txtLevel.setText(getResources().getString(R.string.level2));
        }else if(level==3){
            bgOne.setImageResource(R.drawable.autumn1);
            bgTwo.setImageResource(R.drawable.autumn2);
            txtLevel.setText(getResources().getString(R.string.level3));
        }else if(level==4){
            bgOne.setImageResource(R.drawable.winter1);
            bgTwo.setImageResource(R.drawable.winter2);
            txtLevel.setText(getResources().getString(R.string.level4));
        }else if(level==5){
            bgOne.setImageResource(R.drawable.cave1);
            bgTwo.setImageResource(R.drawable.cave2);
            txtLevel.setText(getResources().getString(R.string.level5));
        }

        final ValueAnimator animator = ValueAnimator.ofFloat(0.0f, 1.0f);
        animator.setRepeatCount(ValueAnimator.INFINITE);
        animator.setInterpolator(new LinearInterpolator());
        animator.setDuration(10000L);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                final float progress = (float) animation.getAnimatedValue();
                final float width = bgOne.getWidth();
                final float translationX = width * progress;
                bgOne.setTranslationX(translationX);
                bgTwo.setTranslationX(translationX - width);
            }
        });
        animator.start();

        final ImageView boy = findViewById(R.id.im_boy);
        boy.setImageResource(R.drawable.boy_animation);
        final AnimationDrawable boy_run =(AnimationDrawable)boy.getDrawable();
        boy_run.start();

        Timer timer=new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                Intent riddleIntent = new Intent(LevelsActivity.this,RiddleActivity.class);
                riddleIntent.putExtra("level",level);
                riddleIntent.putExtra("marks",marks);
                startActivity(riddleIntent);
                finish();
            }
        },9400);

        findViewById(R.id.imbtn_pause).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LevelsActivity.super.onPause();
                Intent mainIntent = new Intent(LevelsActivity.this,MainActivity.class);
                mainIntent.putExtra("level",level);
                mainIntent.putExtra("marks",marks);
                startActivity(mainIntent);
                finish();
            }
        });
    }
}
